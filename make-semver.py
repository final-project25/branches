#!/usr/bin/env python3
import os
import re
import sys
import semver
import subprocess


def git(*args):
    # subprocess.check_output run command with arguments and return it`s output
    return subprocess.check_output(["git"] + list(args))


def tag_repo(tag):
    # os.environ - is a dictionary of environment variables
    url = os.environ["CI_REPOSITORY_URL"]

    # Transforms the repository URL to the SSH URL
    # Example input: https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/final-project25/branches.git
    # Example output: git@gitlab.com:final-project25/branches.git
    push_url = re.sub(r'.+@([^/]+)/', r'git@\1:', url)

    git("remote", "set-url", "--push", "origin", push_url)
    git("tag", tag)
    git("push", "origin", tag)


def bump(latest):
    tags = os.environ["TAGS_MESSAGE"]
    if "#minor" in tags:
        return semver.bump_minor(latest)
    elif "#major" in tags:
        return semver.bump_major(latest)
    else:
        return semver.bump_patch(latest)


def main():
    try:
        latest = git("describe", "--tags").decode().strip()
    except subprocess.CalledProcessError:
        # No tags in the repository
        version = "1.0.0"
    else:
        # Skip already tagged commits
        if '-' not in latest:
            print(latest)
            return 0

        version = bump(latest)

    tag_repo(version)
    print(version)

    return 0


if __name__ == "__main__":
    sys.exit(main())
